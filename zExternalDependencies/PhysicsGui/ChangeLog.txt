/**
* Author: Jamie King
* Project: Physics UI
* Release Date: 5/12/14
*
**/

//TO-DO:
* Add arrows to 3D environment
* Fix mouse grab when moving camera
* Add option to disable grid for better frame rate
* Fix memory leak on 3D mode
* Add option to enable 3D view while program is running

//Changes:

5/28/14 | Andrew Huertas
*	There is a memory leak in 3D mode if memory leak checking is turned on:
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );

5/28/14 | Andrew Huertas
*	Edited change log for request to turn off grid

5/28/14 | Doug Fresh
*	Change log added 

5/28/14 | Jamie King
*	3D environment added