#include <GL\glew.h>
#include "MeglWindow.h"
#include <QtGui\qmouseevent>
#include <QtGui\qkeyevent>
#include "..\..\zExternalDependencies\glm\glm\gtx\compatibility.hpp"
#include "..\..\zExternalDependencies\glm\glm\gtc\matrix_transform.hpp"
#include "..\..\zExternalDependencies\glm\glm\gtx\transform.hpp"
#include <iostream>
#include <fstream>
#include "..\..\NeumontTools\include\ShapeGenerator.h"
#include <Qt\qdebug.h>
#include "Camera.h"
//#include "DEBUG_SLIDER.h"
Camera theCamera;

using glm::vec3;
using glm::mat4;
using std::string;
using namespace std;
DebugSlider* red1;
DebugSlider* green1;
DebugSlider* blue1;
DebugSlider* red2;
DebugSlider* green2;
DebugSlider* blue2;
DebugSlider* red3;
DebugSlider* green3;
DebugSlider* blue3;
DebugSlider* red4;
DebugSlider* green4;
DebugSlider* blue4;
DebugSlider* specularPower;

int cubeIndecies;
int planeIndecies;
int donutIndecies;
int sphereIndecies;

int cubeOffset;
int planeOffset;
int donutOffset;
int sphereOffset;
 float row00=4;
GLuint programID;
GLuint cubeGL;
GLuint DonutGL;
GLuint planeGL;
GLuint sphereGL;

float xAxis=0.0f;
float yAxis=13.5f;
float zAxis=-23.0f;
QCheckBox *thelightBox;
QImage theImage;
	
MeglWindow::MeglWindow(DebugSlider &r1,DebugSlider &g1,DebugSlider &b1,DebugSlider &r2,DebugSlider &g2,DebugSlider &b2,DebugSlider &r3,DebugSlider &g3,DebugSlider &b3,
					   DebugSlider &r4,DebugSlider &g4,DebugSlider &b4,DebugSlider &specPower , QCheckBox &lightBox)
{
	
	
	red1= &r1;
	green1=&g1;
	blue1=&b1;
	red2=&r2;
	green2=&g2;
	blue2=&b2;
	red3=&r3;
	green3=&g3;
	blue3=&b3;
	red4=&r4;
	green4=&g4;
	blue4=&b4;
	specularPower=&specPower;
	thelightBox=&lightBox;
	
}
void MeglWindow::imageLoader()
{
	if(!theImage.load("Copy of Dragon Industries Logo.png"))
	{
		cout<<"You broke it";
	}
	else
	{
		QImage madItImage = QGLWidget::convertToGLFormat(theImage);
		GLuint m_theImage;
		glGenTextures(1,&m_theImage);
		glBindTexture(GL_TEXTURE_2D, m_theImage);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, madItImage.width(), madItImage.height(), 0, GL_BGRA, GL_UNSIGNED_BYTE, madItImage.bits());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	glGenerateMipmap(GL_TEXTURE_2D);
	glEnable(GL_TEXTURE_2D); 

	}
}
void MeglWindow::initializeGL()
{
	setMouseTracking(true);
	glewInit();
	glEnable(GL_DEPTH_TEST);
	SendDataToHardware();
	compileShaders();
	imageLoader();
	connect(&myTimer,SIGNAL(timeout()),this,SLOT(myUpdate()));
	myTimer.start(0);
	
}
void MeglWindow::SendDataToHardware()
{
	Neumont::ShapeData cube=Neumont::ShapeGenerator::makeCube();
	Neumont::ShapeData plane=Neumont::ShapeGenerator::makePlane(10);
	Neumont::ShapeData donut=Neumont::ShapeGenerator::makeTorus(10);
	Neumont::ShapeData sphere=Neumont::ShapeGenerator::makeSphere(10);

	GLuint bufferID;
	glGenBuffers(1,&bufferID);
	glBindBuffer(GL_ARRAY_BUFFER,bufferID);
	
	glBufferData(GL_ARRAY_BUFFER,
		cube.vertexBufferSize() + cube.indexBufferSize()
		+ plane.vertexBufferSize() + plane.indexBufferSize() + 
		donut.vertexBufferSize() + donut.indexBufferSize() +
		sphere.vertexBufferSize()+ sphere.indexBufferSize(),0, GL_STATIC_DRAW);
	GLuint offset=0;
	
	//cube
	glBufferSubData(GL_ARRAY_BUFFER,offset,cube.vertexBufferSize(),cube.verts);
	offset+=cube.vertexBufferSize();
	glBufferSubData(GL_ARRAY_BUFFER,offset,cube.indexBufferSize(),cube.indices);
	offset+=cube.indexBufferSize();

	//plane
	glBufferSubData(GL_ARRAY_BUFFER,offset,plane.vertexBufferSize(),plane.verts);
	offset+=plane.vertexBufferSize();
	glBufferSubData(GL_ARRAY_BUFFER,offset,plane.indexBufferSize(),plane.indices);
	offset+=plane.indexBufferSize();

	// donut
	glBufferSubData(GL_ARRAY_BUFFER,offset,donut.vertexBufferSize(),donut.verts);
	offset+=donut.vertexBufferSize();
	glBufferSubData(GL_ARRAY_BUFFER,offset,donut.indexBufferSize(),donut.indices);
	offset+=donut.indexBufferSize();
	//sphere
	glBufferSubData(GL_ARRAY_BUFFER,offset,sphere.vertexBufferSize(),sphere.verts);
	offset+=sphere.vertexBufferSize();
	glBufferSubData(GL_ARRAY_BUFFER,offset,sphere.indexBufferSize(),sphere.indices);
	offset+=sphere.indexBufferSize();
	
	glGenVertexArrays(1,&cubeGL);
	glGenVertexArrays(1,&planeGL);
	glGenVertexArrays(1,&DonutGL);
	glGenVertexArrays(1,&sphereGL);
	
	//cube
	glBindVertexArray(cubeGL);
	
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glBindBuffer(GL_ARRAY_BUFFER,bufferID);
	glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,Neumont::Vertex::STRIDE,(void*)Neumont::Vertex::POSITION_OFFSET);
	glVertexAttribPointer(1,4,GL_FLOAT,GL_FALSE,Neumont::Vertex::STRIDE,(void*)Neumont::Vertex::COLOR_OFFSET);
	glVertexAttribPointer(2,3,GL_FLOAT,GL_FALSE,Neumont::Vertex::STRIDE,(void*)Neumont::Vertex::NORMAL_OFFSET);
	glVertexAttribPointer(3,2,GL_FLOAT,GL_FALSE,Neumont::Vertex::STRIDE,(void*)Neumont::Vertex::UV_OFFSET);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,bufferID);
	
	////plane
	GLuint planebyteOffset=cube.vertexBufferSize()+cube.indexBufferSize();

	glBindVertexArray(planeGL);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glBindBuffer(GL_ARRAY_BUFFER,bufferID);
	glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,Neumont::Vertex::STRIDE,(void*)planebyteOffset);
	glVertexAttribPointer(1,4,GL_FLOAT,GL_FALSE,Neumont::Vertex::STRIDE,(void*)(planebyteOffset+ sizeof(float)*3));
	glVertexAttribPointer(2,3,GL_FLOAT,GL_FALSE,Neumont::Vertex::STRIDE,(void*)(planebyteOffset+ Neumont::Vertex::NORMAL_OFFSET));
	glVertexAttribPointer(3,2,GL_FLOAT,GL_FALSE,Neumont::Vertex::STRIDE,(void*)(planebyteOffset+ Neumont::Vertex::UV_OFFSET)); 
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,bufferID);
	//donut
	GLuint donutByteOffset=planebyteOffset+plane.vertexBufferSize()+plane.indexBufferSize();
	glBindVertexArray(DonutGL);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glBindBuffer(GL_ARRAY_BUFFER,bufferID);
	glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,Neumont::Vertex::STRIDE,(void*)donutByteOffset);
	glVertexAttribPointer(1,4,GL_FLOAT,GL_FALSE,Neumont::Vertex::STRIDE,(void*)(donutByteOffset+ sizeof(float)*3));
	glVertexAttribPointer(2,3,GL_FLOAT,GL_FALSE,Neumont::Vertex::STRIDE,(void*)(donutByteOffset+Neumont::Vertex::NORMAL_OFFSET));
	glVertexAttribPointer(3,2,GL_FLOAT,GL_FALSE,Neumont::Vertex::STRIDE,(void*)(donutByteOffset+ Neumont::Vertex::UV_OFFSET)); 
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,bufferID);
	//sphere
	GLuint sphereByteOffset=donutByteOffset+ donut.vertexBufferSize() + donut.indexBufferSize();
	glBindVertexArray(sphereGL);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glBindBuffer(GL_ARRAY_BUFFER,bufferID);
	glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,Neumont::Vertex::STRIDE,(void*)sphereByteOffset);
	glVertexAttribPointer(1,4,GL_FLOAT,GL_FALSE,Neumont::Vertex::STRIDE,(void*)(sphereByteOffset+ sizeof(float)*3));
	glVertexAttribPointer(2,3,GL_FLOAT,GL_FALSE,Neumont::Vertex::STRIDE,(void*)(sphereByteOffset+Neumont::Vertex::NORMAL_OFFSET));
	glVertexAttribPointer(3,2,GL_FLOAT,GL_FALSE,Neumont::Vertex::STRIDE,(void*)(sphereByteOffset+ Neumont::Vertex::UV_OFFSET)); 
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,bufferID);

	cubeIndecies=cube.numIndices;
	planeIndecies=plane.numIndices;
	donutIndecies=donut.numIndices;
	sphereIndecies=sphere.numIndices;

	cubeOffset=cube.vertexBufferSize();
	planeOffset= planebyteOffset+plane.vertexBufferSize();
	donutOffset= donutByteOffset+donut.vertexBufferSize();
	sphereOffset= sphereByteOffset + sphere.vertexBufferSize();

	

	

}

string readShaderCode(const char* fileName)
{
	ifstream meInput(fileName);
	if(!meInput.good())
	{
		cout<<"File failed to load..."<<fileName;
		exit(1);
	}
	return string(istreambuf_iterator<char>(meInput),
		istreambuf_iterator<char>());
}
void MeglWindow::compileShaders()
{

	GLuint vertexShaderID=glCreateShader(GL_VERTEX_SHADER);
	
	GLuint fragshaderVertex=glCreateShader(GL_FRAGMENT_SHADER);
	const char* adapter[1];
	string temp=""; 
	if(thelightBox->isChecked())
	{
		temp= readShaderCode("VertexShader.glsl");
	}
	else
	{
		temp=readShaderCode("noLightingVertexShader.glsl");
	}
	adapter[0]=temp.c_str();
	glShaderSource(vertexShaderID,1,adapter,0);
	temp=readShaderCode("FragmentShader.glsl");
	adapter[0]=temp.c_str();
	glShaderSource(fragshaderVertex,1,adapter,0);
	glCompileShader(vertexShaderID);
	glCompileShader(fragshaderVertex);
	programID=glCreateProgram();
	glAttachShader(programID,vertexShaderID);
	glAttachShader(programID,fragshaderVertex);
	glLinkProgram(programID);
	glUseProgram(programID);

	GLint compileStatus;
	glGetShaderiv(vertexShaderID, GL_COMPILE_STATUS,&compileStatus);
	if(compileStatus != GL_TRUE)
	{
		GLint logLength;
		glGetShaderiv(vertexShaderID,GL_INFO_LOG_LENGTH,&logLength);
		char* buffer = new char[logLength];
		GLsizei bitBucket;
		glGetShaderInfoLog(vertexShaderID,logLength,&bitBucket,buffer);
		qDebug()<<buffer;
		delete[] buffer;
	}
	glGetShaderiv(fragshaderVertex, GL_COMPILE_STATUS,&compileStatus);
	if(compileStatus != GL_TRUE)
	{
		GLint logLength;
		glGetShaderiv(fragshaderVertex,GL_INFO_LOG_LENGTH,&logLength);
		char* buffer = new char[logLength];
		GLsizei biteBucket;
		glGetShaderInfoLog(fragshaderVertex,logLength,&biteBucket,buffer);
		qDebug()<<buffer;
		delete[] buffer;
	}

}
void MeglWindow::myUpdate()
{
	//w
	if(GetAsyncKeyState(87))
	{
		yAxis+=.5f;
	}
	//s
	if(GetAsyncKeyState(83))
	{
		yAxis-=.5f;
	}
	//a
	if(GetAsyncKeyState(65))
	{
		xAxis-=.5f;
	}
	//d
	if(GetAsyncKeyState(68))
	{
		xAxis+=.5f;
	}
	//r
	if(GetAsyncKeyState(82))
	{
		zAxis-=.5f;
	}

	if(GetAsyncKeyState(70))
	{
		zAxis+=.5f;
	}
	if(GetAsyncKeyState(VK_UP))
		theCamera.moveFoward();
		
	if(GetAsyncKeyState(VK_DOWN))
		theCamera.movBackward();
	
	if(GetAsyncKeyState(VK_LEFT))
		theCamera.strafeLeft();
	
	if(GetAsyncKeyState(VK_RIGHT))
		theCamera.strafeRight();
	
	if(GetAsyncKeyState(80))
		theCamera.moveUp();
	
	if(GetAsyncKeyState(76))
		theCamera.moveDown();
		glViewport(0,0,width(),height());
	repaint();
	//qDebug()<<player1Score;


}
void MeglWindow::paintGL()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glViewport(0,0,width(),height());
	GLint modelToWorlLocation=glGetUniformLocation(programID,"modelToWorld");
	mat4 modelToWorld;
	GLint transformationLocation= glGetUniformLocation(programID,"transform");
	mat4 transform;
	mat4 projection=glm::perspective(60.0f,((float)width())/((float)height()),0.1f,10.0f);
	vec3 lightPosition(xAxis,yAxis,zAxis);
	



	
		compileShaders();
			GLint ambientLocation = glGetUniformLocation(programID,"ambientLight");
			vec3 ambientlight(red1->value(),green1->value(),blue1->value());
			glUniform3fv(ambientLocation,1,&ambientlight[0]);

			GLint objectColorLocation = glGetUniformLocation(programID,"objectColor");
			vec3 objectColor(red2->value(),green2->value(),blue2->value());
			glUniform3fv(objectColorLocation,1,&objectColor[0]);

			GLint lightColorLocation = glGetUniformLocation(programID,"lightColor");
			vec3 lightColor(red3->value(),green3->value(),blue3->value());
			glUniform3fv(lightColorLocation,1,&lightColor[0]);

			

			GLint lightLocation = glGetUniformLocation(programID,"lightPosition");
			
			glUniform3fv(lightLocation,1,&lightPosition[0]);

			GLint specColLocation = glGetUniformLocation(programID,"specCol");
			vec3 specColPosition(red4->value(),green4->value(),blue4->value());
			glUniform3fv(specColLocation,1,&specColPosition[0]);

			GLint powerLocation = glGetUniformLocation(programID,"power");
			float powerPosition=specularPower->value();
			glUniform1fv(powerLocation,1,&powerPosition);

			GLint eyeLocation = glGetUniformLocation(programID,"eyePosition");
			vec3 eyePosition=theCamera.position;
			glUniform3fv(eyeLocation,1,&eyePosition[0]);
	
	

	glBindVertexArray(cubeGL);
	mat4 scalar =glm::scale(mat4(),vec3(0.06f,0.06f,0.06f));
	mat4 cubeTranslation=glm::translate(mat4(),lightPosition);
	mat4 cubeRotationY=glm::rotate(mat4(),0.0f,vec3(1.0f,0.0f,0.0f));
	mat4 cubeRotationX=glm::rotate(mat4(),0.0f,vec3(0.0f,1.0f,0.0f));
	
	transform=projection*theCamera.getWorldToViewMatrix()*scalar*cubeTranslation*cubeRotationX*cubeRotationY;
	modelToWorld=scalar*cubeTranslation*cubeRotationX*cubeRotationY;
	
	glUniformMatrix4fv(transformationLocation,1,GL_FALSE,&transform[0][0]);
	glUniformMatrix4fv(modelToWorlLocation,1,GL_FALSE,&modelToWorld[0][0]);
	glDrawElements(GL_TRIANGLES,cubeIndecies,GL_UNSIGNED_SHORT,(void*)cubeOffset);

	

	glBindVertexArray(planeGL);
	mat4 planeTranslation=glm::translate(mat4(),vec3(1.0f,-2.0f,-6.0f));
	mat4 planeRotationY=glm::rotate(mat4(),20.0f,vec3(1.0f,0.0f,0.0f));
	mat4 planeRotationX=glm::rotate(mat4(),0.0f,vec3(0.0f,1.0f,0.0f));
	transform=projection*theCamera.getWorldToViewMatrix()*planeTranslation*planeRotationX*planeRotationY;
	modelToWorld=planeTranslation*planeRotationX*planeRotationY;
	glUniformMatrix4fv(transformationLocation,1,GL_FALSE,&transform[0][0]);
	glUniformMatrix4fv(modelToWorlLocation,1,GL_FALSE,&modelToWorld[0][0]);
	glDrawElements(GL_TRIANGLES,planeIndecies,GL_UNSIGNED_SHORT,(void*)planeOffset);

	glBindVertexArray(DonutGL);
	mat4 donutTranslation=glm::translate(mat4(),vec3(-2.0f,-2.0f,-5.0f));
	mat4 donutRotationY=glm::rotate(mat4(),15.0f,vec3(1.0f,0.0f,0.0f));
	mat4 donutRotationX=glm::rotate(mat4(),0.0f,vec3(0.0f,1.0f,0.0f));
	transform=projection*theCamera.getWorldToViewMatrix()*donutTranslation*donutRotationX*donutRotationY;
	glUniformMatrix4fv(transformationLocation,1,GL_FALSE,&transform[0][0]);
	glUniformMatrix4fv(modelToWorlLocation,1,GL_FALSE,&modelToWorld[0][0]);
	glDrawElements(GL_TRIANGLES,donutIndecies,GL_UNSIGNED_SHORT,(void*)donutOffset);

	glBindVertexArray(sphereGL);
	mat4 sphereTranslation=glm::translate(mat4(),vec3(2.0f,-1.5f,-4.0f));
	mat4 sphereRotationY=glm::rotate(mat4(),20.0f,vec3(1.0f,0.0f,0.0f));
	mat4 sphereRotationX=glm::rotate(mat4(),0.0f,vec3(0.0f,1.0f,0.0f));
	transform=projection*theCamera.getWorldToViewMatrix()*sphereTranslation*sphereRotationX*sphereRotationY;
	modelToWorld=sphereTranslation*sphereRotationX*sphereRotationY;
	glUniformMatrix4fv(transformationLocation,1,GL_FALSE,&transform[0][0]);
	glUniformMatrix4fv(modelToWorlLocation,1,GL_FALSE,&modelToWorld[0][0]);
	glDrawElements(GL_TRIANGLES,donutIndecies,GL_UNSIGNED_SHORT,(void*)donutOffset);
}
void MeglWindow::mouseMoveEvent(QMouseEvent* e)
{
	
	theCamera.mouseUpdate(glm::vec2(e->x(),e->y()));
	/*qDebug()<<one->value();
	repaint();*/
}
void MeglWindow::keyPressEvent(QKeyEvent*e)
{
	
	if(GetAsyncKeyState(VK_UP))
		theCamera.moveFoward();
		
	if(GetAsyncKeyState(VK_DOWN))
		theCamera.movBackward();
	
	if(GetAsyncKeyState(VK_LEFT))
		theCamera.strafeLeft();
	
	if(GetAsyncKeyState(VK_RIGHT))
		theCamera.strafeRight();
	
	if(GetAsyncKeyState(80))
		theCamera.moveUp();
	
	if(GetAsyncKeyState(76))
		theCamera.moveDown();
	
	
}




