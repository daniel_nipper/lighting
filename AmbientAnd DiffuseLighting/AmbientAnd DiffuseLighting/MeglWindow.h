#pragma once
#include <QtOpenGL\qglwidget>
#include <Qt\qtimer.h>
#include "..\..\zExternalDependencies\glm\glm\glm.hpp"
#include "..\..\NeumontTools\include\ShapeGenerator.h"
#include <QtGui\QHboxLayout>
#include <QtGui\QVboxLayout>
#include "DEBUG_SLIDER.h"
#include "QtGui\qcheckbox.h"

class MeglWindow : public QGLWidget
{
	Q_OBJECT
	QTimer myTimer;
	void SendDataToHardware();
	void compileShaders();
	void imageLoader();
public:
	MeglWindow(DebugSlider &r1,DebugSlider &g1,DebugSlider &b1,DebugSlider &r2,DebugSlider &g2,DebugSlider 
		&b2,DebugSlider &r3,DebugSlider &g3,DebugSlider &b3,DebugSlider &r4,DebugSlider &g4,DebugSlider &b4,DebugSlider &specPower, QCheckBox &lightBox);
protected:
	
	void initializeGL();
	void paintGL();
	void mouseMoveEvent(QMouseEvent*);
	void keyPressEvent(QKeyEvent*);
	private slots:
	void myUpdate();
	
};

