#include "Camera.h"
#include "..\..\zExternalDependencies\glm\glm\gtx\transform.hpp"


const float MOVEMENT_SPEED =0.5f;

Camera::Camera() : viewDirection(0.0f,-0.5f,-0.9f),position(0.0f,2.0f,-.5f),
	UP(0.0f,1.0f,0.0f)
{
	
}
void Camera::mouseUpdate(const glm::vec2& newMousePosition)
{
	glm::vec2 mouseDelta = newMousePosition - oldMousePosition;
	if(glm::length(mouseDelta)>50.0f)
	{
		oldMousePosition=newMousePosition;
		return;
	}
	const float ROTATIONAL_SPEED = 0.5f;
	strafeDirection = glm::cross(viewDirection, UP);
	glm::mat4x4 rotate = glm::rotate(-mouseDelta.x*ROTATIONAL_SPEED,UP)*glm::rotate(-mouseDelta.y*ROTATIONAL_SPEED,strafeDirection);

	viewDirection = glm::mat3x3(rotate)*viewDirection;
	oldMousePosition=newMousePosition;

}
glm::mat4x4 Camera::getWorldToViewMatrix() const
{
	return glm::lookAt(position,position+viewDirection,UP);
}
void Camera::moveFoward()
{
	position+= MOVEMENT_SPEED*viewDirection;
}
void Camera::movBackward()
{
	position+= -MOVEMENT_SPEED*viewDirection;
}
void Camera::strafeLeft()
{
	position+= -MOVEMENT_SPEED*strafeDirection;
}
void Camera::strafeRight()
{
	position+= MOVEMENT_SPEED*strafeDirection;
}
void Camera::moveUp()
{
	position+= MOVEMENT_SPEED*UP;
}
void Camera::moveDown()
{
	position+= -MOVEMENT_SPEED*UP;
}


