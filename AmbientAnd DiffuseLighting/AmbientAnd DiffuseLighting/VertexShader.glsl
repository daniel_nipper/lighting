#version 400
in layout(location=0) vec3 position;
in layout(location=1) vec4 Color;
in layout(location=2) vec3 normal;
in layout(location=3) vec2 uv;

uniform vec3 ambientLight;
uniform mat4 transform;
uniform vec3 lightPosition;
uniform float power;
uniform vec3 specCol;
uniform vec3 eyePosition;
uniform mat4 modelToWorld;
uniform  vec3 objectColor;
uniform vec3 lightColor;

out vec4 deColor;

out vec2 inUv;
void main()
{
	inUv=uv;
	vec4 p=vec4(position,1);
	vec4 newPosition=transform*p;
	gl_Position=newPosition;
	vec3 tranNorm=mat3(modelToWorld)*normal;
	vec3 tranposNorm=vec3(modelToWorld*vec4(position,1));
	vec3 lightvector=normalize(lightPosition-tranposNorm);
	float brightness=dot(lightvector,tranNorm);
	float brightness2=clamp(brightness,0,1);
	
	vec3 normeye=normalize(eyePosition-tranposNorm);
	
	vec3 nextColor=lightColor*brightness2;
	vec3 reflec=-reflect(lightvector,normal);
	float dotResult= dot(reflec,normeye);
	float sepcbright=clamp(dotResult,0,1);
	float spec=pow(sepcbright,power);
	vec3 newSpecCol=specCol*spec;
	deColor=vec4((nextColor+ambientLight)*objectColor+newSpecCol,1);
}