#pragma once
#include "..\..\zExternalDependencies\glm\glm\glm.hpp"
class Camera
{
	
	
	glm::vec3 viewDirection;
	const glm::vec3 UP;
	glm::vec2 oldMousePosition;
	glm::vec3 strafeDirection;
public:
	glm::vec3 position;
	Camera();
	void mouseUpdate(const glm::vec2& newMousePosition);
	glm::mat4 getWorldToViewMatrix() const;
	void moveFoward();
	void movBackward();
	void strafeRight();
	void strafeLeft();
	void moveUp();
	void moveDown();
};

