#include "MeWidge.h"
#include <QtGui\qpushbutton.h>
#include <QtGui\qlabel.h>
#include "DEBUG_SLIDER.h"
#include <QtGui\qcheckbox.h>
#include "MeglWindow.h"



MeWidge::MeWidge()
{

	QVBoxLayout* mainLayout = new QVBoxLayout();
	setLayout(mainLayout);
	
	QCheckBox *thelightBox= new QCheckBox("Apply Lighting");
	mainLayout->addWidget(thelightBox);
	QHBoxLayout* row1=new QHBoxLayout();
	mainLayout->addLayout(row1);
	DebugSlider* r =new DebugSlider();
	DebugSlider* g =new DebugSlider();
	DebugSlider* b =new DebugSlider();
	r->setValue(1.0f);
	b->setValue(1.0f);
	g->setValue(1.0f);
	QLabel *ambient=new QLabel("Ambient");
	QLabel *ObjectColor=new QLabel("ObjectColor");
	QLabel *lightColor=new QLabel("LightColor");
	QLabel *specExponet=new QLabel("Spec Exponent");
	QLabel *specColor=new QLabel("specColor");
	row1->addWidget(ambient);
	row1->addWidget(r);
	row1->addWidget(g);
	row1->addWidget(b);
	r->setFixedHeight(50);
	g->setFixedHeight(50);
	b->setFixedHeight(50);
	QHBoxLayout* row2=new QHBoxLayout();
		mainLayout->addLayout(row2);
	DebugSlider* r2 =new DebugSlider();
	DebugSlider* g2 =new DebugSlider();
	DebugSlider* b2 =new DebugSlider();
	
	r2->setValue(0.1f);
	g2->setValue(0.5f);
	b2->setValue(0.3f);
	r2->setFixedHeight(50);
	g2->setFixedHeight(50);
	b2->setFixedHeight(50);
	row2->addWidget(ObjectColor);
	row2->addWidget(r2);
	row2->addWidget(g2);
	row2->addWidget(b2);

QHBoxLayout* row3=new QHBoxLayout();
mainLayout->addLayout(row3);
	DebugSlider* r3 =new DebugSlider();
	DebugSlider* g3 =new DebugSlider();
	DebugSlider* b3 =new DebugSlider();
	r3->setValue(0.9f);
	g3->setValue(0.2f);
	b3->setValue(0.4f);
	r3->setFixedHeight(50);
	g3->setFixedHeight(50);
	b3->setFixedHeight(50);
	row3->addWidget(lightColor);
	row3->addWidget(r3);
	row3->addWidget(g3);
	row3->addWidget(b3);
	QHBoxLayout* row4=new QHBoxLayout();
	mainLayout->addLayout(row4);
	DebugSlider* specPower=new DebugSlider(50.0f,1000.0f);
	row4->addWidget(specExponet);
	row4->addWidget(specPower);
	specPower->setFixedHeight(50);

	QHBoxLayout* row5=new QHBoxLayout();
	mainLayout->addLayout(row5);
	DebugSlider* r4 =new DebugSlider();
	DebugSlider* g4 =new DebugSlider();
	DebugSlider* b4 =new DebugSlider();
	r4->setValue(0.3f);
	g4->setValue(0.5f);
	b4->setValue(0.8f);
	r4->setFixedHeight(50);
	g4->setFixedHeight(50);
	b4->setFixedHeight(50);
	row5->addWidget(specColor);
	row5->addWidget(r4);
	row5->addWidget(g4);
	row5->addWidget(b4);

	mainLayout->addWidget(new MeglWindow(*r,*g,*b,*r2,*g2,*b2,*r3,*g3,*b3,*r4,*g4,*b4,*specPower,*thelightBox));

	
	
	

}


