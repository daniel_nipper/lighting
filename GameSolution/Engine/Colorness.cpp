#include "Core.h"
#include "Vector2.h"
#include "Colorness.h"

Core::RGB Colorness::varyColor(Core::RGB color,int varience)
{
	r=GetRValue(color);

	r= r + int(myRand.randomInRange(-(float)varience,(float)varience +1));

	r = r < 0 ? 0:r;
	r = r > 255 ? 255:r;

	g=GetGValue(color);

	g= g + int(myRand.randomInRange(-(float)varience,(float)varience + 1));

	g = g < 0 ? 0:g;
	g = g > 255 ? 255:g;

	b=GetBValue(color);

	b= b + int(myRand.randomInRange(-(float)varience,(float)varience + 1));

	b = b < 0 ? 0:b;
	b = b > 255 ? 255:b;

	return RGB(r,g,b);
}

