#pragma once
#include "ExportHeader.h"
#include <stdlib.h>



class Randomness
{
public:
	
	 
ENGINE_SHARED float randomFloat();
ENGINE_SHARED Vector2D randomUnitVector();
ENGINE_SHARED float randomInRange(float min, float max);
};

