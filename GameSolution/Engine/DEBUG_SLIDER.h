#pragma once
#include <QtGui\qwidget>
#include "ExportHeader.h"
class QSlider;
class QLabel;

class DebugSlider : public QWidget
{
Q_OBJECT

QSlider* slider;
QLabel* label;
float sliderGranularity;
float min;
float max;

private slots:
ENGINE_SHARED void sliderValueChanged();

signals:
ENGINE_SHARED void valueChanged(float newValue);

public:
ENGINE_SHARED DebugSlider(
float min = 0.0f, float max = 1.0f,
bool textOnLeft = false, float granularity = 40.0);
float ENGINE_SHARED value() const;
void ENGINE_SHARED setValue(float newValue);
};
