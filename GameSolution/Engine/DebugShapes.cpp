#if DEBUG_SHAPES_ON
#include "DebugShapes.h"
#include "..\..\NeumontTools\include\ShapeGenerator.h"
#include "..\..\zExternalDependencies\glm\glm\gtx\compatibility.hpp"
#include "..\..\zExternalDependencies\glm\glm\gtc\matrix_transform.hpp"
#include "..\..\zExternalDependencies\glm\glm\gtx\transform.hpp"


GeneralWindows* theRender;
GeneralWindows::ShaderInfo* theShadeInfo[3];
GeneralWindows::GeometryInfo* theSphereGeom[1];
GeneralWindows::GeometryInfo* theCubeGeom[1];

DebugShapes::DebugShapes(GeneralWindows& theRend)
{
	//glEnable(GL_DEPTH_TEST);
	theRender=&theRend;
	
	theShadeInfo[0]= theRender->createShaderInfo("../../GameSolution/Engine/ShapeVertexShader.glsl","../../GameSolution/Engine/DebugFragShader.glsl");
	theShadeInfo[1]= theRender->createShaderInfo("../../GameSolution/Engine/lineVertexShader.glsl","../../GameSolution/Engine/DebugFragShader.glsl");
	theShadeInfo[2]=theRender->createShaderInfo("../../GameSolution/Engine/VertexShaderNoColor.glsl","../../GameSolution/Engine/DebugFragShader.glsl");
	Neumont::ShapeData theSphere= Neumont::ShapeGenerator::makeSphere(30);

	theSphereGeom[0]=theRender->addGeometry(theSphere.verts,theSphere.vertexBufferSize(),theSphere.indexBufferSize(),theSphere.indices,theSphere.numIndices,GL_TRIANGLES);
	theRender->addShaderStreamedParameter(theSphereGeom[0],0,GeneralWindows::ParameterType::PT_VEC3,Neumont::Vertex::STRIDE,Neumont::Vertex::POSITION_OFFSET);

	Neumont::ShapeData theCube= Neumont::ShapeGenerator::makeCube();
	theCubeGeom[0]= theRender->addGeometry(theCube.verts,theCube.vertexBufferSize(),theCube.indexBufferSize(),theCube.indices,theCube.numIndices,GL_TRIANGLES);
	theRender->addShaderStreamedParameter(theCubeGeom[0],0,GeneralWindows::ParameterType::PT_VEC3,Neumont::Vertex::STRIDE,Neumont::Vertex::POSITION_OFFSET);

}


DebugShapes::~DebugShapes(void)
{
}


GeneralWindows::RenderableInfo* theSphereRend[NumSpheres];
float sphereLife[NumSpheres];
int DebugShapes::addSphere(vec3 pos, vec3 col,mat4 transform, float lifeTime, bool depthON=true)
{
	mat4 trans=glm::translate(mat4(),pos)*transform;
	lifeTime;
	int numInArray=0;
	while (theSphereRend[numInArray]!=NULL && numInArray<NumSpheres)
	{
		
		numInArray++;
	}
	sphereLife[numInArray]=lifeTime;
	if(depthON)
	{
	theSphereRend[numInArray]= theRender->addRenderable(theSphereGeom[0],trans,theShadeInfo[0],NULL);
	
	//theRender->addRenderableUniformParameter(theSphereRend[numInArray],"transform",GeneralWindows::ParameterType::PT_MAT4,&theSphereRend[numInArray]->where[0][0]);
	}
	else
	{
		vec3 tempPos=(pos*vec3(1,1,0));
		theSphereRend[numInArray]= theRender->addRenderable(theSphereGeom[0],glm::translate(mat4(),tempPos)*transform,theShadeInfo[0],NULL);
	//theRender->addRenderableUniformParameter(theSphereRend[numInArray],"transform",GeneralWindows::ParameterType::PT_MAT4,&theSphereRend[numInArray]->where[0][0]);
	}
	sphereColors[numInArray]=col;
	
	return numInArray;
}
GeneralWindows::RenderableInfo* DebugShapes::getSphereRend(int rendNum)
{
	return theSphereRend[rendNum];

}
GeneralWindows::RenderableInfo* theCubeRend[20];
vec3 cubeColors[20];
float CubeLife[20];
void DebugShapes::addCube(vec3 pos,vec3 col, mat4 transform,float lifeTime, bool depthON)
{
	mat4 trans=glm::translate(mat4(),pos)*transform;
	lifeTime;
	int numInArray=0;
	while (theCubeRend[numInArray]!=NULL && numInArray<20)
	{
		
		numInArray++;
	}
	CubeLife[numInArray]=lifeTime;
	if(depthON)
	{
	theCubeRend[numInArray]= theRender->addRenderable(theCubeGeom[0],trans,theShadeInfo[0],NULL);
	
	
	}
	else
	{
		vec3 tempPos=(pos*vec3(1,1,0));
		theCubeRend[numInArray]= theRender->addRenderable(theSphereGeom[0],glm::translate(mat4(),tempPos)*transform,theShadeInfo[0],NULL);
	
	}

	cubeColors[numInArray]=col;
	
}
GeneralWindows::GeometryInfo* LineGeom[20];
GeneralWindows::RenderableInfo* theLineRend[20];
float LineLife[20];
void DebugShapes::addLine(vec3 start, vec3 end, glm::vec4 col,float lifeTime,mat4 transform, bool depthON)
{
	lifeTime;

vec3 tepStart=start;
	vec3 tempEnd=end;
	if(!depthON)
	{
		tepStart=start*vec3(1.0f,1.0f,0.0f);
		tempEnd=end*vec3(1.0f,1.0f,0.0f);
	}
	Neumont::Vertex stuff[]= 
	{
		{tepStart,col},
		{tempEnd,col}
		
	};
	GLushort indices[]={0,1};
	unsigned int versize=sizeof(stuff);
	unsigned int numIndices=2;
	uint indSize=sizeof(indices);
	int numInArray=0;
	while (LineGeom[numInArray]!=NULL && numInArray<20)
	{
		
		numInArray++;
	}
	LineLife[numInArray]=lifeTime;
	int stride = Neumont::Vertex::STRIDE;
	int colorOffset = Neumont::Vertex::COLOR_OFFSET;
	LineGeom[numInArray]= theRender->addGeometry(stuff,versize,indSize,indices,numIndices,GL_LINES);
	theLineRend[numInArray]= theRender->addRenderable(LineGeom[numInArray],transform,theShadeInfo[1],NULL);
	theRender->addShaderStreamedParameter(LineGeom[numInArray],0,GeneralWindows::ParameterType::PT_VEC3,stride,0);
	theRender->addShaderStreamedParameter(LineGeom[numInArray],1,GeneralWindows::ParameterType::PT_VEC3,stride,colorOffset);
}
GeneralWindows::GeometryInfo* VectorGeom[20];
GeneralWindows::RenderableInfo* theVectorRend[20];
float VectorLife[20];
int DebugShapes::addVector(vec3 start, vec3 end, glm::vec4 col,float lifeTime,mat4 transform, bool depthON)
{
	lifeTime;

	vec3 tepStart=start;
	vec3 tempEnd=end;
	if(!depthON)
	{
		tepStart=start*vec3(1.0f,1.0f,0.0f);
		tempEnd=end*vec3(1.0f,1.0f,0.0f);
	}
	glm::vec4 lightCol=glm::vec4(1.0f,1.0f,1.0f,1.0f);
	
	Neumont::Vertex stuff[]= 
	{
		{tepStart,col},
		{tempEnd,lightCol}
		
	};
	
	GLushort indices[]={0,1};
	unsigned int versize=sizeof(stuff);
	unsigned int numIndices=2;
	uint indSize=sizeof(indices);
	int numInArray=0;
	while (VectorGeom[numInArray]!=NULL && numInArray<20)
	{
		
		numInArray++;
	}
	VectorLife[numInArray]=lifeTime;
	int stride = Neumont::Vertex::STRIDE;
	int colorOffset = Neumont::Vertex::COLOR_OFFSET;
	VectorGeom[numInArray]= theRender->addGeometry(stuff,versize,indSize,indices,numIndices,GL_LINES);
	theVectorRend[numInArray]= theRender->addRenderable(VectorGeom[numInArray],transform,theShadeInfo[1],NULL);
	theRender->addShaderStreamedParameter(VectorGeom[numInArray],0,GeneralWindows::ParameterType::PT_VEC3,stride,0);
	theRender->addShaderStreamedParameter(VectorGeom[numInArray],1,GeneralWindows::ParameterType::PT_VEC3,stride,colorOffset);
	isVecVisible[numInArray]=true;
	return numInArray;
}
GeneralWindows::GeometryInfo* PointGeom[20];
GeneralWindows::RenderableInfo* thePointRend[20];
float PointLife[20];
void DebugShapes::addPoint(vec3 start, glm::vec4 col,float lifeTime,mat4 transform, bool depthON)
{
	

	vec3 tepStart=start;
	vec3 tempEnd1=start+vec3(0.0f,5.0f,0.0f);
	vec3 tempEnd2=start+vec3(5.0f,0.0f,0.0f);
	vec3 tempEnd3=start+vec3(0.0f,0.0f,5.0f);
	vec3 tempEnd4=start+vec3(0.0f,-5.0f,0.0f);
	vec3 tempEnd5=start+vec3(-5.0f,0.0f,0.0f);
	vec3 tempEnd6=start+vec3(0.0f,0.0f,-5.0f);

	glm::vec4 dark=glm::vec4(0.0f,0.0f,0.0f,0.0f);
	if(!depthON)
	{
		tepStart=start*vec3(1.0f,1.0f,0.0f);
		tempEnd1=start+vec3(0.0f,5.0f,0.0f);
		tempEnd2=start+vec3(5.0f,0.0f,0.0f);
		tempEnd3=start+vec3(0.0f,0.0f,0.0f);
		tempEnd4=start+vec3(0.0f,-5.0f,0.0f);
		tempEnd5=start+vec3(-5.0f,0.0f,0.0f);
		tempEnd6=start+vec3(0.0f,0.0f,0.0f);
	}
	glm::vec4 lightCol=glm::vec4(1.0f,1.0f,1.0f,1.0f);
	
	Neumont::Vertex stuff[]= 
	{
		{start,col},
		{tempEnd1,col},
		{tempEnd2,col},
		{tempEnd3,col},
		{tempEnd4,dark},
		{tempEnd5,dark},
		{tempEnd6,dark},
		
	};
	
	GLushort indices[]={0,1,0,2,0,3,0,4,0,5,0,6};
	unsigned int versize=sizeof(stuff);
	unsigned int numIndices=12;
	uint indSize=sizeof(indices);
	int numInArray=0;
	while (PointGeom[numInArray]!=NULL && numInArray<20)
	{
		
		numInArray++;
	}
	PointLife[numInArray]=lifeTime;

	int stride = Neumont::Vertex::STRIDE;
	int colorOffset = Neumont::Vertex::COLOR_OFFSET;
	PointGeom[numInArray]= theRender->addGeometry(stuff,versize,indSize,indices,numIndices,GL_LINES);
	thePointRend[numInArray]= theRender->addRenderable(PointGeom[numInArray],transform,theShadeInfo[1],NULL);
	theRender->addShaderStreamedParameter(PointGeom[numInArray],0,GeneralWindows::ParameterType::PT_VEC3,stride,0);
	theRender->addShaderStreamedParameter(PointGeom[numInArray],1,GeneralWindows::ParameterType::PT_VEC3,stride,colorOffset);

}
void DebugShapes::drawDebugShapes(mat4 camerasPos)
{
	
	for (int i = 0; i < NumSpheres; i++)
	{
		if(theSphereRend[i] != NULL)
		{
			mat4 trans= camerasPos*theSphereRend[i]->where;
			theRender->addRenderableUniformParameter(theSphereRend[i],"Color",GeneralWindows::ParameterType::PT_VEC3,&sphereColors[i][0]);
			theRender->addRenderableUniformParameter(theSphereRend[i],"transform",GeneralWindows::ParameterType::PT_MAT4,&trans[0][0]);
			glBindVertexArray(theSphereRend[i]->whatGeometryIndex->geomitryID);
			glDrawElements(GL_TRIANGLES,theSphereRend[i]->whatGeometryIndex->geomIndicies,
				GL_UNSIGNED_SHORT,(void*)(theSphereRend[i]->whatGeometryIndex->geometryOffset));
		}
	}


	for (int i = 0; i < 20; i++)
	{
		if(theCubeRend[i] != NULL)
		{
			mat4 trans= camerasPos*theCubeRend[i]->where;
			theRender->addRenderableUniformParameter(theCubeRend[i],"Color",GeneralWindows::ParameterType::PT_VEC3,&cubeColors[i][0]);
			theRender->addRenderableUniformParameter(theCubeRend[i],"transform",GeneralWindows::ParameterType::PT_MAT4,&trans[0][0]);
			glBindVertexArray(theCubeRend[i]->whatGeometryIndex->geomitryID);
			glDrawElements(GL_TRIANGLES,theCubeRend[i]->whatGeometryIndex->geomIndicies,
				GL_UNSIGNED_SHORT,(void*)(theCubeRend[i]->whatGeometryIndex->geometryOffset));
		}
	}
	
	for (int i = 0; i < 20; i++)
	{
		if(theLineRend[i] != NULL)
		{
			mat4 trans= camerasPos*theLineRend[i]->where;
			theRender->addRenderableUniformParameter(theLineRend[i],"transform",GeneralWindows::ParameterType::PT_MAT4,&trans[0][0]);
			glBindVertexArray(theLineRend[i]->whatGeometryIndex->geomitryID);
			glDrawElements(GL_LINES,theLineRend[i]->whatGeometryIndex->geomIndicies,
				GL_UNSIGNED_SHORT,(void*)(theLineRend[i]->whatGeometryIndex->geometryOffset));
		}
	}
	for (int i = 0; i < 20; i++)
	{
		if(theVectorRend[i] != NULL)
		{
			if(isVecVisible[i])
			{
				mat4 trans= camerasPos*theVectorRend[i]->where;
				theRender->addRenderableUniformParameter(theVectorRend[i],"transform",GeneralWindows::ParameterType::PT_MAT4,&trans[0][0]);
				glBindVertexArray(theVectorRend[i]->whatGeometryIndex->geomitryID);
				glDrawElements(GL_LINES,theVectorRend[i]->whatGeometryIndex->geomIndicies,
					GL_UNSIGNED_SHORT,(void*)(theVectorRend[i]->whatGeometryIndex->geometryOffset));
			}
		}
	}
	for (int i = 0; i < 20; i++)
	{
		if(thePointRend[i] != NULL)
		{
			mat4 trans= camerasPos*thePointRend[i]->where;
			theRender->addRenderableUniformParameter(thePointRend[i],"transform",GeneralWindows::ParameterType::PT_MAT4,&trans[0][0]);
			glBindVertexArray(thePointRend[i]->whatGeometryIndex->geomitryID);
			glDrawElements(GL_LINES,thePointRend[i]->whatGeometryIndex->geomIndicies,
				GL_UNSIGNED_SHORT,(void*)(thePointRend[i]->whatGeometryIndex->geometryOffset));
		}
	}
}
void DebugShapes::DebugShapeUpadte()
{
	float time=.005;
	for (int i = 0; i < NumSpheres; i++)
	{
		if(sphereLife[i] != NULL)
		{
			sphereLife[i]-=time;
			if(sphereLife[i]<=0)
			{
				theSphereRend[i]=NULL;
			}

		}
	}


	for (int i = 0; i < 20; i++)
	{
		if(CubeLife[i] != NULL)
		{
			CubeLife[i]-=time;
			if(CubeLife[i]<=0)
			{
				theCubeRend[i]=NULL;
			}

		}
	}
	
	for (int i = 0; i < 20; i++)
	{
		if(LineLife[i] != NULL)
		{
			LineLife[i]-=time;
			if(LineLife[i]<=0)
			{
				theLineRend[i]=NULL;
			}

		}
	}
	for (int i = 0; i < 20; i++)
	{
		if(VectorLife[i] != NULL)
		{
			VectorLife[i]-=time;
			if(VectorLife[i]<=0)
			{
				theVectorRend[i]=NULL;
			}

		}
	}
	for (int i = 0; i < 20; i++)
	{
		if(PointLife[i] != NULL)
		{
			PointLife[i]-=time;
			if(PointLife[i]<=0)
			{
				thePointRend[i]=NULL;
			}

		}
	}
}
void DebugShapes::removeShapes()
{
	for(int i=0; i<NumSpheres;i++)
	{
		theSphereRend[i]=NULL;
	}
}
void DebugShapes::removeSingleSpher(int i)
{
	theSphereRend[i]=NULL;
}
void DebugShapes::removeSingleVector(int i)
{
	theVectorRend[i]=NULL;
}
GeneralWindows::GeometryInfo* coneGeometry;
GeneralWindows::GeometryInfo* stemGeometry;
//DebugShapes::VectorGraphic DebugShapes::addVector2(
//	const glm::vec3& from, const glm::vec3& to, float lengthDelta)
//{
//
//	if(coneGeometry == 0)
//	{
//		Neumont::ShapeData cone=Neumont::ShapeGenerator::makeCone();
//		Neumont::ShapeData stem=Neumont::ShapeGenerator::makeCylinder();
//		coneGeometry = theRender->addGeometry(cone.verts,cone.vertexBufferSize(),cone.indexBufferSize(),cone.indices,cone.numIndices,GL_TRIANGLES);
//		stemGeometry =	theRender->addGeometry(stem.verts,stem.vertexBufferSize(),stem.indexBufferSize(),stem.indices,stem.numIndices,GL_TRIANGLES);
//	}
//	VectorGraphic ret;
//	ret.coneRenderable = theRender->addRenderable(
//		coneGeometry, glm::mat4(), 
//		theShadeInfo[2],NULL);
//	ret.stemRenderable = theRender->addRenderable(
//		stemGeometry, glm::mat4(), 
//		theShadeInfo[2],NULL);
//	ret.setEndPoints(from, to, lengthDelta);
//	return ret;
//}
//const float CONE_HEIGHT = 0.35f;
//mat4 DebugShapes::createBasisFromOneVector(vec3 xBasis)
//{
//	vec3 normalY(0.0f,1.0f,0.0f);
//	vec3 normalZ(0.0f,0.0f,1.0f);
//	vec3 normalX(xBasis);
//	vec3 u1=glm::cross(normalX,normalY);
//	vec3 v=glm::cross(normalX,u1);
//	return mat4(glm::mat3(v,u1,u2));
//
//
//}
//void DebugShapes::VectorGraphic::setEndPoints(
//	const glm::vec3& from, const glm::vec3& to, float lengthDelta)
//{
//	DebugShapes theDeBug;
//	glm::vec3 theLine = to - from;
//	glm::vec3 xBasis = glm::normalize(theLine);
//	glm::mat4 theRotation=theDeBug.createBasisFromOneVector(xBasis);
//	
//	const float cylinderWeightLoss = 0.02f;
//	glm::vec3 backOff = (lengthDelta / 2) * xBasis;
//	const float CONE_SCALE = 0.3f;
//	glm::vec3 coneHeight = CONE_HEIGHT * CONE_SCALE * xBasis;
//	stemRenderable->where =  // scale then rotate then translate
//		glm::scale(glm::length(theLine) - lengthDelta - glm::length(coneHeight), 
//		cylinderWeightLoss, cylinderWeightLoss)*theRotation *
//		glm::translate(from + backOff);
//	coneRenderable->where = 
//		glm::translate(to - backOff - coneHeight) * 
//		theRotation *
//		glm::scale(CONE_SCALE, CONE_SCALE, CONE_SCALE);
//}


#endif