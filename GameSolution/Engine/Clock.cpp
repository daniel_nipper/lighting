#include "Clock.h"
#include "Logger.h"

void Clock::start()
{

	QueryPerformanceCounter(&timeLastFrame);
}

bool Clock::initialize()
{

	if(!QueryPerformanceFrequency(&timeFrequency)!=0)
	{
		return false;
	}

	return true ;
}

bool Clock::shutDown()
{
	return true;
}

float Clock::lap()
{
	float fps;
	fps=stop();
	start();
	return fps;
}

float Clock::stop()
{
	LARGE_INTEGER thisTime;
	QueryPerformanceCounter(&thisTime);
	LARGE_INTEGER delta;
	delta.QuadPart = thisTime.QuadPart-timeLastFrame.QuadPart;
	deltaTimeRecip=((float)timeFrequency.QuadPart)/((float)delta.QuadPart);
	deltaTime=((float)delta.QuadPart)/((float)timeFrequency.QuadPart);
	
	return deltaTimeRecip;
	
}
float Clock::miliSeconds()
{
	return deltaTime;
}



