#version 400
in layout(location=0) vec3 position;
in layout(location=1) vec3 Color;
uniform mat4 transform;


out vec3 deColor;

void main()
{
vec4 p= vec4(position,1);
vec4 newPosition= transform*p;
gl_Position=newPosition;
deColor=Color;
}