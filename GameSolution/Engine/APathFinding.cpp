#include "APathFinding.h"


APathFinding::APathFinding()
{
}


APathFinding::~APathFinding()
{
}

Game::GameNode* APathFinding::findAStarPath(Game::GameNode* theGameNodes,const int numNodes)
{
	
	int forRandomNum=numNodes+1;
	do{
		chosenEnd=rand() % forRandomNum;
		chosenStart=rand() % forRandomNum;
	}while(chosenStart !=chosenEnd);
	Game::GameNode tempStart=theGameNodes[chosenStart];
	Game::GameNode tempEnd=theGameNodes[chosenEnd];	

	return theGameNodes;
}
int fromStart=0;
Game::GameNode* APathFinding::findAStarPathWithStartAndEnd(Game::GameNode* theGameNodes,Game::GameNode &startNode,Game::GameNode &endNode, const int numNodes)
{
	delete aNodes;
	delete returnNodes;
	fromStart=0;
	Game::GameNode tempStart=startNode;
	Game::GameNode tempEnd=endNode;	
	totalNodes=numNodes;
	aNodes=new AStarNode[numNodes];
	convertGameToAstar(numNodes,theGameNodes,tempStart,tempEnd);
	evaluatingNode=start;
	
	int positionTemp=evaluatingNode.gameNode-theGameNodes;
	while(!(evaluatingNode.gameNode->position==end.gameNode->position))
	{
		int removedNode=-1;
		
		addToOpenList(theGameNodes);
		closed.append(evaluatingNode);
		//evaluatingNode=open.at(0);
		for (int i = 0; i < open.count(); i++)
		{
			for (int j = 0; j < open.count(); j++)
			{
				if(open.at(i).tec<=open.at(j).tec)
				{
					evaluatingNode=open.at(i);
					removedNode=i;
				}
			}
			

		}
		if(removedNode!=-1)
		{
			open.removeAt(removedNode);
		}
	
	}
	AStarNode temp=evaluatingNode;
	count=1;
	while(temp.csf!=-1)
	{
		temp=*temp.parent;
		if(temp.csf!=-1)
		{
			count++;
		}
	}
	returnNodes=new Game::GameNode[count];

	 temp=evaluatingNode;
	int reverseCount=count;
	while(temp.csf!=-1)
	{
		reverseCount-=1;
		returnNodes[reverseCount]=*temp.gameNode;
		temp=*temp.parent;
		
	}
	fromStart=0;
	return returnNodes;
	
}



void APathFinding::addToOpenList(Game::GameNode* theGameNodes)
{
	AStarNode theTemp=evaluatingNode;
	for (int i = 0; i < evaluatingNode.gameNode->numConections; i++)
			{
				Game::Connection& c=evaluatingNode.gameNode->connections[i];
				AStarNode temp=aNodes[c.target-theGameNodes];
				if(fromStart==0)
				{
					temp.csf=c.cost;
				
				}
				else
				{
					temp.csf=evaluatingNode.csf+c.cost;
				}
				for (int j = 0; j < totalNodes; j++)
				{
					if(evaluatingNode.gameNode->position==aNodes[j].gameNode->position)
					{
						temp.parent=&aNodes[j];
					}
				}
				
				temp.tec=temp.csf+temp.hiristic;
				open.append(temp);

		


			}
	fromStart++;
}
Game::GameNode* APathFinding::findAStarPathWithStartNode(Game::GameNode* theGameNodes,Game::GameNode &startNode,int numNodes,const int startNodeNum)
{
	Game::GameNode tempStart=startNode;
	int forRandomNum=numNodes+1;


	do{
		chosenEnd=rand() % forRandomNum;
	}while(chosenEnd !=startNodeNum);
	Game::GameNode tempEnd=theGameNodes[chosenEnd];
	aNodes=new AStarNode[numNodes];
	return returnNodes;
	
}
Game::GameNode* APathFinding::findAStarPathWithEndNode(Game::GameNode* theGameNodes,Game::GameNode &endNode, const int numNodes,int endNodeNum)
{
	Game::GameNode tempEnd=endNode;
	int forRandomNum=numNodes+1;


	do{
		chosenStart=rand() % forRandomNum;
	}while(chosenStart !=endNodeNum);
	Game::GameNode tempStart=theGameNodes[chosenStart];
	
	return returnNodes;
}
void APathFinding::convertGameToAstar(int numNodes, Game::GameNode* theGameNodes,Game::GameNode &tempStart, Game::GameNode &tempEnd)
{
	zero.csf=-1;
	zero.hiristic=0;
	zero.tec=0;
	zero.parent=NULL;
	start.gameNode=&tempStart;
	start.csf=0;
	start.hiristic=glm::length(tempEnd.position-tempStart.position);
	zero.gameNode=start.gameNode;
	start.parent=&zero;
	start.tec=start.hiristic;
	end.gameNode=&tempEnd;
	end.hiristic=0;
	aNodes[0]=start;
	aNodes[(numNodes-1)]=end;
	for (int i = 1; i < (numNodes-1); i++)
	{
		aNodes[i].gameNode=&theGameNodes[i];
		aNodes[i].hiristic=glm::length(tempEnd.position-theGameNodes[i].position);
		
	}
	

}