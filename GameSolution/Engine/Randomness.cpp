#include "Vector2.h"
#include "Randomness.h"

const float TWO_PI=2*3.14159f;
float Randomness::randomFloat()
{
	return (float)rand()/RAND_MAX;
}
Vector2D Randomness::randomUnitVector()
{
	float angle=TWO_PI*randomFloat();
	Vector2D vector(cos(angle),sin(angle));
	return vector;
}
float Randomness::randomInRange(float min, float max)
{
	return randomFloat()*(max - min + 1) + min;
}

