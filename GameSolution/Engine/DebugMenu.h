#pragma once
#include <QtGui\QHboxlayout>
#include <QtGui\QVboxlayout>
#include <QtGui\qpushbutton.h>
#include <QtGui\qlabel.h>
#include <QtGui\qcheckbox.h>
#include "Debug_Slider.h"
#include "ExportHeader.h"
#include "Clock.h"
class DebugMenu  : public QWidget
{

public:

	Clock theClock;
	QVBoxLayout *Layout;
	QVBoxLayout *sliderInfo;
	QVBoxLayout *checkBoxLayout;

ENGINE_SHARED DebugMenu(QWidget m);
ENGINE_SHARED ~DebugMenu();
ENGINE_SHARED void watchFloat(const char* varableName, float &floatWatched);
ENGINE_SHARED void slideFloat(const char* varableName, float &floatAltered);
ENGINE_SHARED bool toggleBool(const char* varableName);
ENGINE_SHARED void updateValues();

};

