#include "DebugMenu.h"
#include <sstream>








std::string convertToString(float f)
{
	std::stringstream s;
	s << f;
	return s.str();
}

QHBoxLayout *watchRow = new QHBoxLayout();
QHBoxLayout *row = new QHBoxLayout();
QHBoxLayout *row2 = new QHBoxLayout();
	
DebugMenu::DebugMenu(QWidget m)
{
	theClock.initialize();
	theClock.start();
	Layout = new QVBoxLayout();
	sliderInfo = new QVBoxLayout();
	checkBoxLayout = new QVBoxLayout();

	setLayout(Layout);
	setLayout(sliderInfo);
	setLayout(checkBoxLayout);

	QLabel* fpsLabel = new QLabel("FPS: ");

	QLabel *fps = new QLabel(convertToString(theClock.stop()).c_str());

	QHBoxLayout *row = new QHBoxLayout();
	Layout -> addLayout(row);
	row -> addWidget(fpsLabel);
	row -> addWidget(fps);
	Layout-> addLayout(watchRow);
	Layout ->addLayout(row);
	Layout -> addLayout(row2);
}
DebugMenu::~DebugMenu()
{
}
void DebugMenu::watchFloat(const char* name, float &watched)
{
	
	
	QLabel* watchedVarable = new QLabel(name);
	QLabel* varable  = new QLabel(convertToString(watched).c_str());
	watchRow -> addWidget(watchedVarable);
	watchRow -> addWidget(varable);
}

void DebugMenu::slideFloat(const char* name, float &watched)
{
	
	QLabel* SliderVarableLabel = new QLabel(name);
	DebugSlider* SliderVarableAltered = new DebugSlider();
	
	
	row->addWidget(SliderVarableLabel);
	row->addWidget(SliderVarableAltered);
	watched = SliderVarableAltered -> value();

}
bool DebugMenu::toggleBool(const char* name)
{
	
	QCheckBox* boolCheckBox = new QCheckBox(name); 
	row2 -> addWidget(boolCheckBox);
	bool isChecked;
	isChecked = boolCheckBox -> isChecked();
	return isChecked;
}
 void DebugMenu::updateValues()
 {

 }
