#pragma once
#include "ExportHeader.h"
#include <Qt\qlist.h>
#include "..\..\Levelproj\Game\Game.h"

class APathFinding
{
	int chosenStart;
	int chosenEnd;
public:
	struct AStarNode
	{
		Game::GameNode* gameNode;
		float hiristic;
		float tec;
		float csf;
		AStarNode* parent;
		
	};
	
	AStarNode* aNodes;
	QList<AStarNode> open;
	QList<AStarNode> closed;
	AStarNode start;
	AStarNode zero;
	AStarNode end;
	AStarNode evaluatingNode;
	Game::GameNode* returnNodes;
	int totalNodes;
	int count;

	ENGINE_SHARED	APathFinding();
	ENGINE_SHARED ~APathFinding();
	ENGINE_SHARED Game::GameNode* findAStarPath(Game::GameNode* theGameNodes, const int numNodes);
	ENGINE_SHARED Game::GameNode* findAStarPathWithStartAndEnd(Game::GameNode* theGameNodes,Game::GameNode &startNode,Game::GameNode &endNode, const int numNodes);
	ENGINE_SHARED Game::GameNode* findAStarPathWithStartNode(Game::GameNode* theGameNodes,Game::GameNode &startNode,const int numNodes,int startNodeNum);
	ENGINE_SHARED Game::GameNode* findAStarPathWithEndNode(Game::GameNode* theGameNodes,Game::GameNode &endNode, const int numNodes, int endNodeNum);
	ENGINE_SHARED void addToOpenList(Game::GameNode* theGameNodes);
	ENGINE_SHARED void convertGameToAstar(int numNodes, Game::GameNode* theGameNodes,Game::GameNode &tempStart, Game::GameNode &tempEnd);

};

