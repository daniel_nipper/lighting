#ifndef Profile_h
#define Profile_h
#if PROFILING_ON
#include "Clock.h"
#endif

class Profile
{
#if PROFILING_ON
	const char* category;
	Clock clockOne;
#endif
public:
#if PROFILING_ON
	ENGINE_SHARED Profile(const char* category);
	ENGINE_SHARED ~Profile();
#else

	ENGINE_SHARED Profile(const char* category){}
	ENGINE_SHARED ~Profile(){}
#endif
};
#if PROFILING_ON
#define PROFILE(category) Profile myProfile(category)
#else
#define PROFILE(category)
#endif

#endif
