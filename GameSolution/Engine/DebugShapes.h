
#pragma once
#include "GeneralWindows.h"
#include <QtOpenGL\qglwidget>
#include "ExportHeader.h"
using glm::vec3;
using glm::mat4;

const int NumSpheres=50;

class DebugShapes
{
	
public:
vec3 sphereColors[NumSpheres];
bool isVecVisible[20];

	ENGINE_SHARED DebugShapes(){}
	//class _declspec(dllexport) VectorGraphic
	//{
	//	friend class DebugShapes;
	//	
	//public:
	//	GeneralWindows::RenderableInfo* coneRenderable;
	//	GeneralWindows::RenderableInfo* stemRenderable;
	//	void setEndPoints(const glm::vec3& from, const glm::vec3& to, float lengthDelta);
	//	void setVisible(bool visible);
	//	void dispose();
	//};
	
#if DEBUG_SHAPES_ON

	
	ENGINE_SHARED DebugShapes(GeneralWindows& theRend);
	ENGINE_SHARED ~DebugShapes(void);
	ENGINE_SHARED int addSphere(vec3 pos, vec3 col,mat4 transform, float lifeTime, bool depthON);
	ENGINE_SHARED void addCube(vec3 pos,vec3 col, mat4 transform,float lifeTime, bool depthON);
	ENGINE_SHARED void addLine(vec3 start, vec3 end, glm::vec4 col,float lifeTime,mat4 transform, bool depthON);
	ENGINE_SHARED int addVector(vec3 start, vec3 end, glm::vec4 col,float lifeTime,mat4 transform, bool depthON);
	ENGINE_SHARED void addPoint(vec3 start, glm::vec4 col,float lifeTime,mat4 transform, bool depthON);
	ENGINE_SHARED void drawDebugShapes(mat4 camerasPos);
	ENGINE_SHARED void removeSingleSpher(int i);
	ENGINE_SHARED void removeSingleVector(int i);
	ENGINE_SHARED GeneralWindows::RenderableInfo* getSphereRend(int rendNum);
	
	ENGINE_SHARED void DebugShapeUpadte();
	//ENGINE_SHARED DebugShapes::VectorGraphic addVector2(const glm::vec3& from, const glm::vec3& to, float lengthDelta);
	
	//ENGINE_SHARED mat4 createBasisFromOneVector(vec3 xBasis);
	ENGINE_SHARED void removeShapes();
#else
	ENGINE_SHARED DebugShapes(GeneralWindows& theRend){}
	ENGINE_SHARED ~DebugShapes(void){}
	ENGINE_SHARED void removeSingleVector(int i){}
	ENGINE_SHARED int addSphere(vec3 pos, vec3 col,mat4 transform, float lifeTime, bool depthON){}
	ENGINE_SHARED GeneralWindows::RenderableInfo* getSphereRend(int rendNum){}
	ENGINE_SHARED void addCube(vec3 pos,vec3 col, mat4 transform,float lifeTime, bool depthON){}
	ENGINE_SHARED void addLine(vec3 start, vec3 end, glm::vec4 col,float lifeTime,mat4 transform, bool depthON){}
	ENGINE_SHARED int addVector(vec3 start, vec3 end, glm::vec4 col,float lifeTime,mat4 transform, bool depthON){}
	ENGINE_SHARED void addPoint(vec3 start, glm::vec4 col,float lifeTime,mat4 transform, bool depthON){}
	ENGINE_SHARED void drawDebugShapes(mat4 camerasPos){}
	ENGINE_SHARED void removeSingleSpher(int i){}
	ENGINE_SHARED void DebugShapeUpadte(){}
	ENGINE_SHARED void removeShapes(){}
	//ENGINE_SHARED mat4 createBasisFromOneVector(vec3 xBasis){}
	//ENGINE_SHARED DebugShapes::VectorGraphic addVector2(const glm::vec3& from, const glm::vec3& to, float lengthDelta){}
	
#endif
};

