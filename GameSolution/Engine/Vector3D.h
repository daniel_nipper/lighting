#ifndef Vector3_H
#define Vector3_H
struct Vector3D
{
	 float x;
	 float y;
	 float z;

	Vector3D(float x=0,float y=0,float z=0): x(x), y(y),z(z)
	{

	}
operator float*()
{

	return &x;
}
inline friend Vector3D operator +(const Vector3D& left, const Vector3D& right);
inline friend Vector3D operator -(const Vector3D& left, const Vector3D& right);
inline friend Vector3D operator *(float scalar, const Vector3D& right); 
};
inline Vector3D operator +(const Vector3D& left, const Vector3D& right)
{
	return Vector3D(left.x + right.x,left.y + right.y,left.z+right.z);
}
inline Vector3D operator -(const Vector3D& left, const Vector3D& right)
{
	return Vector3D(left.x - right.x,left.y - right.y,left.z-right.z);
}
inline Vector3D operator *(float scalar,const Vector3D& right )
{
	return Vector3D(scalar* right.x,scalar* right.y,scalar*right.z);
}

#endif
