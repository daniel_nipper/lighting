#include <GL\glew.h>
#pragma once
#pragma warning ( disable : 4201)
#pragma warning ( disable : 4127)
#include <QtOpenGL\qglwidget>
#include "..\..\zExternalDependencies\glm\glm\glm.hpp"
#include "ExportHeader.h"
#include <QtGui\qimage.h>
#include "..\..\zExternalDependencies\glm\glm\gtx\compatibility.hpp"
#include "..\..\zExternalDependencies\glm\glm\gtc\matrix_transform.hpp"
#include "..\..\zExternalDependencies\glm\glm\gtx\transform.hpp"


const int MAX_RENDERABLE=100;
const int MAX_BUFFERS=10;
const int MAX_GEOMETRIES=25;
const int MAX_SHADER=25;
const int MAX_TEXTUREINFO=25;
class GeneralWindows 
{
	
public:
	enum ParameterType
	{
		// These values matter:
		PT_FLOAT = sizeof(float) * 1,
		PT_VEC2 = sizeof(float) * 2,
		PT_VEC3 = sizeof(float) * 3,
		PT_VEC4 = sizeof(float) * 4,
		PT_MAT3 = sizeof(float) * 9,
		PT_MAT4 = sizeof(float) *16,
	};
	
	
	struct  BufferInfo
	{
		GLuint glBufferID;
		GLuint remainingSize;

	}meBufferInfo[MAX_BUFFERS];
	
public:
	struct ShaderInfo
	{
		friend class GeneralWindows;
	public:
		GLuint programID;
		
	}shaderInfos[MAX_SHADER];

	struct GeometryInfo
	{
		friend class GeneralWindows;
	public:
			unsigned int bufferInfoID;
			GLuint bufferOffset;
			GLuint geomitryID;
			GLuint indexingMode;
			GLuint geometryOffset;
			int geomIndicies;
	}geometryInfos[MAX_GEOMETRIES];

	struct TextureInfo
	{
		GLuint textureID;
	}textureInfos[MAX_TEXTUREINFO];


	struct RenderableInfo
	{
		 GeometryInfo* whatGeometryIndex;
		ShaderInfo* howShaderIndex;
		glm::mat4 where;
		bool visible;
		TextureInfo* textureID;
	private:
		friend class GeneralWindows;
		GLuint numUniformParameters;
		
	}renderInfos[MAX_RENDERABLE];
ENGINE_SHARED	GeneralWindows();
	ENGINE_SHARED ~GeneralWindows();

	
	ENGINE_SHARED GeometryInfo* addGeometry(
		const void * verts, 
		GLuint vertexDataSize,
		GLuint indexDataSize,
		ushort* indices, uint numIndices,
		GLuint indexingMode);

	ENGINE_SHARED ShaderInfo* createShaderInfo(
		 const char* vertexShaderFileName,
		const char* fragmentShaderFileName);

	ENGINE_SHARED RenderableInfo* addRenderable(
		GeometryInfo* whatGeometry,
		const glm::mat4& whereMatrix,
		ShaderInfo* howShaders,
		TextureInfo* texture);
	
	ENGINE_SHARED void addShaderStreamedParameter(
		GeometryInfo* thgeom,
		uint layoutLocation, 
		GeneralWindows::ParameterType parameterType,
		uint bufferOffset,
		uint bufferStride);

	ENGINE_SHARED uint addRenderableUniformParameter(
		RenderableInfo* renderable,
		const char* name,
		GeneralWindows::ParameterType parameterType, 
		const float* value);

	ENGINE_SHARED GeneralWindows::TextureInfo* addTexture(const char* fileName);
	ENGINE_SHARED void remake();
	
};

