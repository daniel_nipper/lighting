#ifndef Planets_H
#define Matrix2_H


class Planets
{
public:

	Vector3D Planetposition;
	Vector3D velocity;
	float vectorMag;
	
	Vector3D orbitVectors[4];
	Planets(float x, float y, float drawingScale);
	Planets(Vector2D &point, float drawingScale);
	Planets(Vector3D &point, float drawingScale);
	void Draw(Core::Graphics &);
	void Update(float);
	void spawn(Vector3D position, int count, float angle, Core::Graphics&);

};
#endif