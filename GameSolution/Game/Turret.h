#ifndef Turret_H
#define Turret_H
#include "SpaceShip.h"
#include "Bullet.h"
#include "Enemy.h"
#include "MemoryDebug.h"

	class Turret
	{
	public:
		Matrix3D turrentMatrix;
		Vector2D shipPosition;
		Matrix3D shipTranslate;
		Matrix3D turretTransformations;
		Vector2D normalizeShipToMouse;
		int bulletNumber;
		int turretlength;
		int Score;
		int NumOfLives;
		_CrtMemState test1;
		Turret(int numlines);
		~Turret();
		void draw(Core::Graphics& graphics);
		void Update(float dt);
		void bulletsHittingEnemy();
		void enemiesHitPlayer();
		bool colided(Vector2D first, Vector2D second, int comparer);
	};
#endif