#ifndef Asteroid_H
#define Asteroid_H
//#include "GameObject.h"

class Asteroid: public GameObject
	{
	public:
	Asteroid(int numLines,Vector2D* outLine);
	~Asteroid();
	void Draw(Core::Graphics& graphics);
	void Update(float dt);
};
#endif