#ifndef Game_H
#define Game_H
#include "Assert.h"
#include"Profiler.h"
#include "Profile.h"
#include "Turret.h"

#include "Planets.h"
#include "ParticleEffect.h"



class Game
{
	public:
	
		Game();
		~Game();
		void Update(float dt);
		void Draw(Core::Graphics& graphics);
};
#endif