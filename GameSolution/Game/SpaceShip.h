#pragma once
#include "GameObject.h"


class SpaceShip:public GameObject
{
	

public:
	float angle; 
	Matrix3D transformations;
	float yIncrDec;
	float xIncrDec;
	Vector2D straightUpShip;
	Vector2D *playerShip;
	SpaceShip(int numLines,Vector2D* outLine);

	void Draw(Core::Graphics& graphics);
	void Update(float dt);
	void boundrcolided();
	Vector2D getShipPosition();

};